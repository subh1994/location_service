from django.contrib.gis.geos import Point

def to_boolean(value):
    if value and value.lower() in ('t', 'true', '1'):
        return True
    return False

def get_ref_location(x_geo_lat, x_geo_lang):
    error_message = {'status': 'error', 'message': 'Please provide X-GEO-LATITUDE and X-GEO-LONGITUDE in header'}
    if not (x_geo_lat or x_geo_lang):
        return False, error_message, None
    else:
        return True, None, Point(float(x_geo_lang), float(x_geo_lat), srid=4326)