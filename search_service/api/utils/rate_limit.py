from ratelimit.mixins import RatelimitMixin
from django.views.generic import View

class RateLimitView(RatelimitMixin, View):
    ratelimit_key = 'ip'
    ratelimit_rate = '100/m'
    ratelimit_block = False
    ratelimit_method = ('GET', 'POST')

    def dispatch(self, request, *args, **kwargs):
        pass
