from .utils.rate_limit import RateLimitView
from django.http.response import JsonResponse
from django.contrib.gis.db.models.functions import Distance
from ..models import City
from .utils import to_boolean
from .utils import get_ref_location


class SearchCity(RateLimitView):

    def __init__(self, *args, **kwargs):
        super(SearchCity).__init__(*args, **kwargs)
        self.http_method_names = ['GET']

    def dispatch(self, request, *args, **kwargs):
        ctxt = dict()
        self._data = ctxt
        request = self.request
        search_key = request.GET.get('search_key')
        page = int(request.GET.get('page', 1))
        result_per_page = int(request.GET.get('result_per_page', 5))
        fuzzy_search = to_boolean(request.GET.get('fuzzy_search'))
        x_geo_lat = request.META.get('HTTP_X_GEO_LATITUDE')
        x_geo_lang = request.META.get('HTTP_X_GEO_LONGITUDE')
        success, message,  self.ref_location = get_ref_location(x_geo_lat, x_geo_lang)
        if not success:
            return JsonResponse(message, status=400)

        if search_key:
            self._data.update(self.search_city(search_key, page, result_per_page, fuzzy_search))

        return JsonResponse(self._data)

    def search_city(self, search_key, page, result_per_page, fuzzy_search=False):
        res = {
            'search_keyword': search_key,
            'page': page,
            'result_per_page': result_per_page,
            'result': []
        }
        start_with = (page - 1) * result_per_page
        if fuzzy_search:
            cities = City.objects.filter(name__contains=search_key).annotate(
                distance=Distance("location", self.ref_location)).order_by("distance")[
                     start_with:start_with + result_per_page]
        else:
            cities = City.objects.filter(name__startswith=search_key).annotate(
                distance=Distance("location", self.ref_location)).order_by("distance")[
                     start_with:start_with + result_per_page]
        for city in cities:
            res['result'].append(city.to_json())
        return res
