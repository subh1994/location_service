# location_service


## POSTMAN COLLECTION
[![Run in Postman](https://run.pstmn.io/button.svg)](https://app.getpostman.com/run-collection/7072c271aa092de853a7#?env%5BLocalhost%5D=W3sia2V5IjoiYmFzZV91cmwiLCJ2YWx1ZSI6ImxvY2FsaG9zdDo4MDAwIiwiZGVzY3JpcHRpb24iOiIiLCJ0eXBlIjoidGV4dCIsImVuYWJsZWQiOnRydWV9XQ==)


## Installation
This Repository use Django==1.11 with Postgres database. To store location data it is using GeoDjango please visit [here](https://docs.djangoproject.com/en/2.1/ref/contrib/gis/install/) for installation instructions. 
pipfile is attached with repo to install python module. 