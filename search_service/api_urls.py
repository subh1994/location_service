from django.conf.urls import url
from .api.search_city import SearchCity
from .api.search_locals import SearchLocals


urlpatterns = [
    url(r'^search-city/', SearchCity.as_view()),
    url(r'^search-locals/', SearchLocals.as_view()),
]

