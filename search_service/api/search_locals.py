from .utils.rate_limit import RateLimitView
from django.http.response import JsonResponse
from ..models import Locality
from django.contrib.gis.geos import Point
from django.contrib.gis.db.models.functions import Distance
from django.contrib.gis.measure import D


class SearchLocals(RateLimitView):

    def __init__(self, *args, **kwargs):
        super(SearchLocals).__init__(*args, **kwargs)
        self.http_method_names = ['GET']

    def dispatch(self, request, *args, **kwargs):
        ctxt = dict()
        self._data = ctxt
        request = self.request
        latitude = request.GET.get('latitude')
        longitude = request.GET.get('longitude')
        if not (latitude or longitude):
            return JsonResponse({'status': 'error', 'message': 'Please provide latitude and longitude in request'}, status=400)

        self._data.update(self.search_locals(latitude, longitude))

        return JsonResponse(self._data)

    def search_locals(self, latitude, longitude):
        res = {
            'latitude': latitude,
            'longitude': longitude,
            'result': []
        }
        ref_location = Point(float(longitude), float(latitude))
        locals = Locality.objects.filter(location__distance_lte=(ref_location, D(m=10000))).annotate(
            distance=Distance("location", ref_location)).order_by("distance")

        total_result = 0
        for local in locals:
            total_result += 1
            res['result'].append(local.to_json())

        res['total_result'] = total_result
        return res
