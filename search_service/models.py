from django.contrib.gis.db import models
from django.contrib.gis.geos import *
from django.contrib.gis.measure import D


class Location(models.Model):
    location = models.PointField(null=False, blank=False, srid=4326, verbose_name="Location")
    latitude = models.FloatField(max_length=50, null=True, db_index=True)
    longitude = models.FloatField(max_length=50, null=True, db_index=True)

    class Meta:
        abstract = True


# Create your models here.
class City(Location):
    name = models.CharField(max_length=100, db_index=True, null=False)
    created_on = models.DateTimeField(auto_now_add=True)
    modified_on = models.DateTimeField(auto_now=True, null=True)
    state = models.CharField(max_length=100, null=True)

    def save(self, *args, **kwargs):
        location = Point(kwargs.get('longitude'), kwargs.get('latitude'))
        self.location = location
        super(City, self).save(**kwargs)

    def __str__(self):
        return self.name

    def to_json(self):
        return dict(
            name=self.name,
            latitude=self.latitude,
            longtitude=self.longitude,
            state=self.state
        )


class Locality(Location):
    name = models.CharField(max_length=200, db_index=True, null=False)
    city = models.ForeignKey(City, related_name='city')
    created_on = models.DateTimeField(auto_now_add=True)
    modified_on = models.DateTimeField(auto_now=True, null=True)

    def save(self, *args, **kwargs):
        location = Point(kwargs.get('longitude'), kwargs.get('latitude'))
        self.location = location
        super(Locality, self).save(**kwargs)

    def __str__(self):
        return self.name

    def to_json(self):
        return dict(
            name=self.name,
            city=self.city.name,
            latitude=self.latitude,
            longtitude=self.longitude,
            state=self.city.state
        )
